#!/bin/bash

echo "Lecture d'un fichier contenu dans le repertoire dir_b"
cat dir_b/test_b.txt
if [$? -eq 0]; then 
    echo "Lecture du fichier dir_b/test_b : OK"
else
    echo "Lecture du fichier dir_b/test_b : FAILURE"
fi

echo "Lecture d'un fichier contenu dans le repertoire dir_c"
cat dir_c/test_c.txt
if [$? -eq 0]
then 
    echo "Lecture du fichier dir_c/test_c.txt : OK"
else
    echo "Lecture du fichier dir_c/test_c.txt : FAILURE"
fi

echo "Créer un fichier dans le dossier dir_b"
touch dir_b/test_b2.txt
if [$? -eq 0] 
then 
    echo "Création du fichier dir_b/test_b2.txt : OK"
else
    echo "Création du fichier dir_b/test_b2.txt : FAILURE"
fi

echo "Créer un nouveau répertoire dans le dossier dir_b"
mkdir dir_b/newD
if [$? -eq 0] 
then 
    echo "Création d'un nouveau répertoire dans le dossier dir_b : OK"
else
    echo "Création d'un nouveau répertoire dans le dossier dir_b : FAILURE"
fi

echo "Modifier les fichiers du dossier dir_c"
echo "hello" >> dir_c/test_c.txt 
if [$? -ne 0]
then 
    echo "Ne peut pas modifier un fichier dans dir_c : OK"
else 
    echo "Ne peut pas modifier un fichier dans dir_c : FAILLURE"
fi

echo "Suprimer les fichiers du dossier dir_c"
rm -r dir_c/test_c.txt 
if [$? -ne 0]
then 
    echo "Ne peut pas supprimer un fichier dans dir_c : OK"
else 
    echo "Ne peut pas supprimer un fichier dans dir_c : FAILLURE"
fi

echo "Créer un fichier dans le dossier dir_c"
touch dir_c/test_c2.txt
if [$? -ne 0] 
then 
    echo "Ne peux pas créer de fichier dans dir_c : OK"
else
    echo "Ne peux pas créer de fichier dans dir_c : FAILURE"
fi

echo "Renommer un fichier dans le dossier dir_c"
mv dir_c/test_c.txt dir_c/nv_test_c.txt
if [$? -ne 0] 
then 
    echo "Ne peux pas renommer de fichier dans dir_c : OK"
else
    echo "Ne peux pas renommer de fichier dans dir_c : FAILURE"
fi

echo "Modifier les fichiers du dossier dir_a"
echo "hello" >> dir_a/test_a.txt 
if [$? -ne 0]
then 
    echo "Ne peut pas modifier un fichier dans dir_a : OK"
else 
    echo "Ne peut pas modifier un fichier dans dir_a : FAILLURE"
fi

echo "Lecture d'un fichier contenu dans le repertoire dir_a"
cat dir_a/test_a.txt
if [$? -ne 0]
then 
    echo "Ne peut pas lire un fichier dans dir_a : OK"
else
    echo "Ne peut pas lire un fichier dans dir_a: FAILURE"
fi

echo "Suprimer les fichiers du dossier dir_a"
rm -r dir_a/test_a.txt 
if [$? -ne 0]
then 
    echo "Ne peut pas supprimer un fichier dans dir_a : OK"
else 
    echo "Ne peut pas supprimer un fichier dans dir_a : FAILLURE"
fi

echo "Créer un fichier dans le dossier dir_a"
touch dir_a/test_a2.txt
if [$? -ne 0] 
then 
    echo "Ne peux créer de fichier dans dir_a : OK"
else
    echo "Ne peux créer de fichier dans dir_a : FAILURE"
fi