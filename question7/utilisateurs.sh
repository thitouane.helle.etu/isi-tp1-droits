#!/bin/bash

##utilisateur admin

sudo adduser admin

##creation du groupe
sudo addgroup groupe_a
sudo addgroup groupe_b

##creation des utilisateurs
sudo adduser lamdba_a
sudo adduser lamdba_b

sudo usermod -g groupe_a lamdba_a
sudo usermod -g groupe_b lamdba_b

##dossier dir_a
mkdir dir_a
sudo chown adminn:groupe_a dir_a
##donne les droit de lecture et d'ecriture aux membres du groupes et proprietaire des fichiers
chmod ug+rw dir_a 
##supprime ces droits aux autres utilisateurs
chmod o-rwx dir_a
## définit l'ID de groupe (setgid) sur le répertoire en cours
chmod g+s dir_a
chmod +t dir_a
touch dir_a/test_a.txt

##dossier dir_b
mkdir dir_b
sudo chown adminn:groupe_b dir_b
chmod g+rw dir_b
chmod o-rwx dir_b
chmod g+s dir_b
chmod +t dir_b
touch dir_b/test_b.txt

##dossier dir_c
mkdir dir_c
sudo chown admin:admin dir_c
chmod u+rwx dir_c
chmod o-w dir_c
touch dir_c/test_c.txt
