#!/bin/bash

echo "Lecture d'un fichier contenu dans le repertoire dir_c"
cat dir_c/test_c.txt
if [$? -eq 0]; then 
    echo "Lecture du fichier dir_c/test_c : OK"
else
    echo "Lecture du fichier dir_c/test_c : FAILURE"
fi

echo "Lecture d'un fichier contenu dans le repertoire dir_a et dir_b"
cat dir_a/test_a.txt && cat dir_b/test_b.txt
if [$? -eq 0]; then 
    echo "Lecture des fichiers de dir_a et dir_b : OK"
else
    echo "Lecture des fichiers de dir_a et dir_b : FAILURE"
fi

echo "Créer un fichier dans le dossier dir_c"
touch dir_c/test_c2.txt
if [$? -eq 0] 
then 
    echo "Création du fichier dir_c/test_c2.txt : OK"
else
    echo "Création du fichier dir_c/test_c2.txt : FAILURE"
fi

echo "Modifier les fichiers du dossier dir_c"
echo "hello" >> dir_c/test_c.txt 
if [$? -eq 0]
then 
    echo "Peut modifier un fichier dans dir_c : OK"
else 
    echo "Peut modifier un fichier dans dir_c : FAILLURE"
fi

echo "Suprimer les fichiers des dossiers dir_a et dir_b et dir_c"
rm -r dir_a/test_a2.txt && rm -r dir_b/test_b2.txt && rm -r dir_c/test_c2.txt
if [$? -eq 0]
then 
    echo "Peut supprimer un fichier dans dir_a et dir_b et dir_c : OK"
else 
    echo "Peut supprimer un fichier dans dir_a et dir_b et dir_c : FAILLURE"
fi


echo "Renommer un fichier dans les dossiers dir_a et dir_b et dir_c"
mv dir_c/test_c.txt dir_c/nv_test_c.txt && mv dir_a/test_a.txt dir_a/nv_test_a.txt && mv dir_b/test_b.txt dir_b/nv_test_b.txt
if [$? -eq 0] 
then 
    echo "Peux renommer des fichiers dans dir_a et dir_b et dir_c : OK"
else
    echo "Peux renommer des fichiers dans dir_a et dir_b et dir_c : FAILURE"
fi

