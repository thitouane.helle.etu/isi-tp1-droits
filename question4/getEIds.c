#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    printf("EUID : %d\n", geteuid());
    printf("EGID : %d\n", getegid());
    return 0;
}