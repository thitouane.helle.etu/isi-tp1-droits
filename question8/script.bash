#!/bin/bash
sudo -u admin -c "touch dir_a/file1.txt; touch dir_b/file2.txt; 
gcc rmg.c -o rmg;
chgrp groupe_c rmg; chmod u+s rmg;"

sudo -u lambda_a -c "rm dir_a/file1.txt; 
./rmg dir_a/file1.txt;"

sudo -u lambda_b -c "rm dir_b/file2.txt; 
./rmg dir_b/file2.txt;"