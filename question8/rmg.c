#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "check_pass.h"

int main(int argc, char *argv[]) {
    struct stat info;
    gid_t gid;
    uid_t uid;
    char *file;
    char password[20], i = 0, c;

    gid = getgid();
    uid = getuid();
    file = argv[1];
    
    stat(file, &info);

    if (gid != info.st_gid) {
        printf("Vous ne pouvez pas supprimer le ficher %s\n", file);
        return EXIT_FAILURE;
    }

    printf("Veuillez entrer votre mot de pass: ");
    scanf("%s", password);

    if (check_pass("/home/admin/passwd", uid, password) == 0) {
        if (!remove(file))
            printf("%s supprmié !\n", file);
        else
            printf("error while removing %s\n", file);
    }
    else {
        printf("Mot de passe incorrect !\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}