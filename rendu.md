# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Helle, Thitouane, email: thitouane.helle.etu@univ-lille.fr

- Kaddeche, Amel, email: amel.kaddeche.etu@univ-lille.fr

## Question 1
Commande :
sudo adduser toto
sudo addgroup toto ubuntu

Ici, toto ne peut pas écrire, il n'a le droit que de lire le fichier

## Question 2

- Le caractère x d'un répertoire signifie qu'on a le droit de se rendre dans sa hièrarchie
- bash: cd: mydir: Permission denied
    car comme vu à la question précédente nous n'avons pas le droit d'accès à la hiérarchie.
- total 0

<br/>d????????? ? ? ? ?      ? .
<br/>d????????? ? ? ? ?      ? ..
<br/>-????????? ? ? ? ?      ? data.txt

    toto n'a pas accès à la hiérarchie donc il ne peut la visualiser avec une commande.

## Question 3

<<<<<<< HEAD
EUID : 1001
EGID : 1001
RUID : 1001
RGID : 1001
non Segmentation fault
=======
EUID : 1000
<br/>EGID : 1000
<br/>RUID : 1000
<br/>RGID : 1000
<br/>non Segmentation fault
>>>>>>> 91cdfbdd55959ef503b9e978c1e056213ce0939e

Après activation du flag set-user-id, toto arrive à lire le fichier:
EUID : 1000
EGID : 1001
RUID : 1001
RGID : 1001
File opens correctly
## Question 4

<<<<<<< HEAD
EUID : 1001
EGID : 1001
=======
Commande pour activer le set-user-id:
`chmod u+s q4.py`
<br/>EUID = 1000
<br/>EGID = 1000
>>>>>>> 91cdfbdd55959ef503b9e978c1e056213ce0939e

## Question 5

- La commande chfn sert à changer les informations d'un utilisateur
<br/>-rwsr-xr-x 1 root root 85064 May 28 2020 /usr/bin/chfn
l'utilsateur root à tout les droits
le groupe root et les autres utilisateurs peuvent executer et lire le fichier
- le fichier a bien été mis à jour

## Question 6

Les mots de passe sont stockés dans etc/shadow, ils sont cryptés et ne peuvent être lu que par root.
Ils sont stockés à part pour plus de sécurité.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.
Oui il est possible de créer d'autres groupes/utilisateurs mais pas necessaire sauf pour les utilisateurs groupe_a groupe_b qui nous permettent de gérer le groupe.

Commande pour exécuter les scripts :
</br> `sudo -u lambda_a ./script_a.bash`
</br> `sudo -u lambda_b ./script_b.bash`
</br> `sudo -u admin .script_admin.bash`
  
drwxrws--t  2 groupe_a groupe_a 4096 Jan 25 13:45 dir_a  
drwxrws--t  2 groupe_b groupe_b 4096 Jan 25 13:47 dir_b  
drwxr-xr-x  2 groupe_c groupe_c 4096 Jan 25 13:50 dir_c  

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








