/*
Pour obtenir la valeur de ces identifiants, le processus peut utiliser les appels système 
getuid() and getgid() pour les real ids, et geteuid() and getegid() pour les ids effectifs. 
Pour obtenir la liste des groupes supplémentaires, il faut utiliser getgroups().
(EUID, EGID, RUID, RGID)
mydir/mydata.txt
*/

/* echo "insert text here" > getIds.c */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    FILE *f;
    
    printf("EUID : %d\n", geteuid());
    printf("EGID : %d\n", getegid());
    printf("RUID : %d\n", getuid());
    printf("RGID : %d\n", getgid());
    printf("Hello world\n");
    
    f = fopen("/mydir/data.txt", "r");
    
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    
    printf("File opens correctly\n");
    fclose(f);
    exit(EXIT_SUCCESS);
    return 0;
}