#include "check_pass.h"

int main(int argc, char *argv[]) {
    char *file;
    register struct passwd *mdp_u;
    register struct passwd *mdp_f;
    struct stat info;
    struct group *file_gr;
    int ngroups;
    gid_t *groups;
    struct group *gr;
    char mdp[200];
    
    if (argc != 2) {
        printf("Un seul fichier accepté.\n");
        return -1;
    }

    file = argv[1];

    mdp_u = getpwuid(getuid());
    if (pw_user == NULL){
        return -1;
    }

    stat(file, &info);
    *file_gr = getgrgid(info.st_gid);
    if (file_gr == NULL){
        printf("%s n'existe pas \n", file);
        return -1;
    }

    ngroups = getgrouplist(mdp_u->pw_name, mdp_u->pw_gid, NULL, &ngroups);
    *groups = groups[ngroups];
    getgrouplist(mdp_u->pw_name, mdp_u->pw_gid, groups, &ngroups);

    for (int i = 0; i < ngroups; i++)  {
        *gr = getgrgid(groups[i]);
        if (strcmp(gr->gr_name, file_gr->gr_name) == 0) {
            printf("Mot de passe contenue in /home/admin/passwd: ");
            scanf("%s", mdp);
            if (check_pass(mdp, pw_user->pw_name) == 0) {
                if(remove(file)) {
                    printf("Erreur dans la suppression du fichier.\n");
                } else {
                    printf("Fichier supprimé.\n");
                }
            } else {
                printf("Mot de passe incorrect.\n");
            }
            return 0;
        }
    }
    printf("Le fichier est dans un groupe différent du votre.\n");
    return -1;
}