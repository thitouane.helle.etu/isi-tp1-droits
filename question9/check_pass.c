#include "check_pass.h"
 
int check_pass(char *mdp, char *login){
    FILE *passwd = fopen("/home/admin/passwd" , "r");
    char[] buffer;

    if (passwd == NULL) {
        printf("fichier %s ne peut etre ouvert.\n", "/home/admin/passwd");
        return -1;
    }
    
    while (fgets(buffer, 20, passwd)) {
        buffer = strchr(buffer,"\n");
        if (strcmp(login, buffer) == 0) {
            fgets(buffer, 20, passwd);
            buffer = strchr(buffer,"\n");
            if (strcmp(mdp, buffer) == 0) {
                fclose(passwd);
                return 0;
            }
        }
    }

    fclose(passwd);
    return -1;
}